var socket = io.connect('http://192.168.10.153:8000');

socket.on('welcome', function () {
  $('#updates').append('<li>Bem vindo ao SNEP Client v2, o cliente WEB do SNEP\n para Callcenter. </li>');
});

socket.on('panel', function () {
  $('#updates').append(http);
});

socket.on('user in', function (data) {
  $('#updates').append('<li>O usuario <strong>' + data.userid + '</strong> tambem esta conectado no SClient</li>');
});

socket.on('name changed', function(data){
  $('#updates').append('<li>Seu ramal foi registrado como: <strong>' + data.nome + '</strong></li>');
});

socket.on('user changed name', function(data){
  $('#updates').append('<li>O usuario <strong>' + data.userid + '</strong>, esta usando agora o ramal <strong>' + data.nome + '</strong>!</li>');
});

socket.on('message sent', function(data){
  $('#chat ul').append('<li><strong>Eu: </strong>' + data.message + '</li>');
});

socket.on('message sent by user', function(data){
  $('#chat ul').append('<li><strong>' + data.nome + ': </strong>' + data.message + '</li>');
});


$(function(){
  $('#form-alterar-nome').submit(function(){
    var nome = $('#nome').val();
    socket.emit('change name', {nome: nome});
    return false;
  });

  $('#form-enviar-mensagem').submit(function(){
    var mensagem = $('#mensagem').val();
    socket.emit('send message', {message: mensagem})
    return false;
    });
  
});
