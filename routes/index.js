var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  res.render('auth', { title: 'SNEP Client v2' });
});


/* GET home page. */
router.get('/chat', function(req, res) {
  res.render('chat', { title: 'SNEP Client v2' });
});

router.get('/panel', function(req, res) {
  res.render('panel', { title: 'SNEP Monitor - Status Panel' });
});


module.exports = router;
